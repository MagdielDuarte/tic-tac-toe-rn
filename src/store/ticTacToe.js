import immutable from 'object-path-immutable'

export const initialState = {
  gameBoard: [['', '', ''], ['', '', ''], ['', '', '']],
  currentPlayer: 'x',
  winner: '',
  status: 'idle'
}

function checkWinner(gameBoard) {
  return ''
}

function reducer(state, action) {
  switch (action.type) {
    case 'START':
      return immutable.set(state, 'status', 'playing');
    case 'SELECT_INDEX': {
      return immutable
        .wrap(state)
        .update(
          `gameBoard.${action.payload.lineIndex}.${action.payload.columnIndex}`,
          (v) => state.currentPlayer
        )
        .set('winner', checkWinner(state.gameBoard))
        .set('currentPlayer', state.currentPlayer === 'x' ? 'o' : 'x' )
        .value()
    }
    default:
      throw new Error();
  }
}

export default reducer