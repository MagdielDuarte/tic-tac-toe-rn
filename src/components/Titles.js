import React from 'react'
import { StyleSheet, Text } from 'react-native';


export function Title({ children }) {
  return <Text style={styles.title}>
    {children}
  </Text>
}

export function CurrentPlayer({ children }) {
  return <Text style={styles.currentPLayer}>
    {children}
  </Text>
}

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#d3d3d3'
  },
  currentPLayer: {
    fontSize: 14,
    marginBottom: 20
  }
});