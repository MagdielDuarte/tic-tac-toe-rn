import React from 'react'
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const GRID_SIZE = 3
const GRID_LIST = new Array(GRID_SIZE).fill(0)

function isLastItem(index, list) {
  return index === list.length - 1
}

function Grid({ gameBoard, dispatch }) {
  return (
    <View style={styles.container}>
      {GRID_LIST.map((_, lineIndex) => (
        <View
          key={lineIndex}
          style={{
            ...styles.line,
            ...isLastItem(lineIndex, GRID_LIST)
              ? styles.lastLine
              : {}
          }}
        >
          {GRID_LIST.map((_, columnIndex) => (
            <TouchableOpacity
              style={styles.column}
              key={columnIndex}
              onPress={() => dispatch({
                type: 'SELECT_INDEX',
                payload: {
                  lineIndex,
                  columnIndex
                }
              })}
            >
              <Text style={styles.text}>
                {gameBoard[lineIndex][columnIndex]}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    padding: 30
  },
  line: {
    flexDirection: 'row',
    borderRightWidth: 2,
  },
  lastLine: {
    borderBottomWidth: 2,
  },
  column: {
    flex: 1,
    height: 100,
    borderTopWidth: 2,
    borderLeftWidth: 2,
    borderColor: 'black',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 70
  }
});

export default Grid;