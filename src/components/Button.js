import React from 'react'
import { StyleSheet, TouchableOpacity, Text} from 'react-native';


function Button({ onPress, title }) {
  return <TouchableOpacity
    style={styles.button}
    onPress={onPress}
  >
    <Text style={styles.text}>{title}</Text>
  </TouchableOpacity>
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'blue',
    padding: 20,
    borderRadius: 3
  },
  text: {
    color: 'white',
    fontSize: 16,
    textTransform: 'uppercase'
  }
});

export default Button