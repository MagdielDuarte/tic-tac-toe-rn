export function getRandomGuess(gameBoard) {
  const availablePlaces = gameBoard.reduce((prev, next, index) => {
    let availableColumns = []
    next
      .forEach((it, colIndex) => {
        if (it === '')  {
          availableColumns.push(`${index}.${colIndex}`)
        }
      })
    return  [...prev, ...availableColumns]
  }, [])

  return availablePlaces.length
    ? availablePlaces[0].split('.')
    : []
}