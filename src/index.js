import React, { useReducer, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import { Title, CurrentPlayer } from  './components/Titles'
import Button from './components/Button'
import Grid from "./components/Grid";
import reducer, { initialState } from './store/ticTacToe'
import { getRandomGuess } from './guesses'

export default function App() {
  const [state, dispatch] = useReducer(reducer, initialState)

  useEffect(() => {
    if(state.currentPlayer === 'o' && state.winner === '') {
      const [lineIndex, columnIndex] = getRandomGuess(state.gameBoard)
      dispatch({
        type: 'SELECT_INDEX',
        payload: { lineIndex, columnIndex }
      })
    }
  }, [state, state, dispatch])

  return (
    <View style={styles.container}>
      <Title>
        TIC TAC TOE!
      </Title>
      <Grid
        gameBoard={state.gameBoard}
        dispatch={dispatch}
      />
      <CurrentPlayer>{state.currentPlayer === 'x'
        ? 'it\'s your turn': 'it\'s CPU\'s turn'}</CurrentPlayer>
      <Button
        title="Star Game"
        onPress={() => {
          dispatch({ type: 'START' })
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 100,
    paddingBottom: 100
  }
});
